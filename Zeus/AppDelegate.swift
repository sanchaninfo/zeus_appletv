/***
 **Module Name:  AppDelegate
 **File Name :  AppDelegate.swift
 **Project :   Zeus
 **Copyright(c) : Zeus.
 **Organization : Peafowl Inc
 **author :  Vijay Bhaskar
 **author :  Manikumar
 **license :
 **version :  1.0.0
 **Created on :
 **Last modified on:
 **Description : AppDelegate, Project starts from here.
 */

import UIKit
import KeychainAccess

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var isFirstTime:Bool?
    @objc var deviceId,uuid,userId,status:String!
    @objc var bundleID = "deviceId"
    @objc var venderId: String!
    @objc var uuidofDevice:String!
    @objc var udid = UIDevice.current.identifierForVendor?.uuidString
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        gotoCode()
        return true
    }
    
    @objc func gotoCode()
    {
        if isConnectedToNetwork() == false
        {
            let alertView = CustomAlertView(frame: CGRect(x: 0, y: 0, width: 1920, height: 1080))
            alertView.setTitle(message: "No Network Detected Please check the Internet connection")
            self.window?.makeKeyAndVisible()
            self.window?.addSubview(alertView)
        }
        else
        {
            let activityView = ActivityView.init(frame:(self.window?.frame)!)
            self.window?.addSubview(activityView)
            venderId = getVenderId()
            if venderId == ""
            {
               setVenderId()
            }
           let url = kActivationCodeUrl + "appname=\(kAppName)&model=AppleTV4Gen&manufacturer=Apple&device_name=AppleTV&device_id=\(venderId!)&device_mac_address=mac&brand_name=Apple&host_name=app&display_name=apple&serial_number=1234&version=\(kAppName)"
       
            ApiManager.sharedManager.postDataWithJsonLambda(url: url, parameters: ["appname":"\(kAppName)"]){
                (responseDict, error, isDone)in
                if error == nil
                {
                    let dict = responseDict as! NSDictionary
                    if dict.object(forKey: "statusCode") != nil
                    {
                        let status = dict.object(forKey: "statusCode") as! Int
                        if status == 200
                        {
                            let result = dict.object(forKey: "result") as! NSDictionary
                            self.uuid = result.value(forKey: "uuid") as! String
                            self.deviceId = result.value(forKey: "device_id") as! String
                       
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            if !((result["pair_devce"]! as! String).contains("active"))
                            {
                                let rootView = storyBoard.instantiateViewController(withIdentifier: "PPCode") as! PPCodeViewController
                                rootView.uuid = self.uuid
                                rootView.deviceId = self.deviceId
                                rootView.code = result.value(forKey: "code") as! String
                                let navigationController = UINavigationController(rootViewController: rootView)
                                self.window?.rootViewController = navigationController
                                self.window?.makeKeyAndVisible()
                            }
                            else
                            {
                                let uservalue = self.getUserId()
                                if (uservalue != "")
                                {
                                    self.userId = uservalue
                                    self.gotoMenu(userid: self.userId,deviceid: self.deviceId)
                                }
                                else
                                {
                                  self.getaccesstoken()
                                }
                            }
                        }
                        else
                        {
                            let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                                UIAlertAction in
                                exit(0)
                            })
                            alertview.addAction(defaultAction)
                            self.window?.rootViewController?.present(alertview, animated: true, completion: nil)
                            
                        }

                    }
                    else
                    {
                        let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                            UIAlertAction in
                            exit(0)
                        })
                        alertview.addAction(defaultAction)
                        self.window?.rootViewController?.present(alertview, animated: true, completion: nil)
 
                    }
                }
                else
                {
                    let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                    let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                        UIAlertAction in
                        exit(0)
                    })
                    alertview.addAction(defaultAction)
                    self.window?.rootViewController?.present(alertview, animated: true, completion: nil)
                }
                
                activityView.removeFromSuperview()
            }
        }
    }
    
    @objc func getaccesstoken()
    {
        let url = kgetaccesstoken + "appname=\(kAppName)&uuid=\(self.uuid!)&deviceID=\(self.deviceId!)"
        let parameters = [String:String]()
        ApiManager.sharedManager.postDataWithJsonLambda(url: url , parameters: parameters){
            (responseDict,error,isDone)in
            if error == nil
            {
                let dict = responseDict as! NSDictionary
                if dict["statusCode"] != nil
                {
                    let status = dict["statusCode"] as! Int
                    if status == 200
                    {
                     let result = dict["result"] as!NSDictionary
                     let token = result["token"] as! String
                      self.userId = token
                      gUserID = token
                      self.setUserId()
                      self.gotoMenu(userid: self.userId, deviceid: self.deviceId)
                    }
                    else
                    {
                       print("json error")
                    }
                }
                else
                {
                    print("json error")
                    
                }
            }
            else
            {
                print(error?.localizedDescription ?? "json error in getaccess token")
            
        }
    }
    }
    
  //  getaccesstoken
  //  appName ,uuid , deviceID
    
    //setVenderId
    @objc func setVenderId() {
        let keychain = Keychain(service: bundleID)
        do {
            try keychain.set(udid!, key: bundleID)
            print("venderId set : key \(bundleID) and value: \(udid!)")
            venderId = getVenderId()
        }
        catch let error {
            print("Could not save data in Keychain : \(error)")
        }
        print(venderId)
    }
    
    
    // getVenderId
    @objc func getVenderId() -> String {
        
        let keychain = Keychain(service: bundleID)
        
        if try! keychain.get(bundleID) != nil
        {
            let token : String = try! keychain.get(bundleID)!
            
            return token
            
        }
        return ""
        
    }
    //getUserid
    @objc func getUserId() -> String {
        
        let keychain = Keychain(service: "userId")
        
        if try! keychain.get("userId") != nil
        {
            let token : String = try! keychain.get("userId")!
            
            return token
        }
        return ""
        
    }
    @objc func setUserId() {
        let keychain = Keychain(service: "userId")
        do {
            try keychain.set(gUserID, key: "userId")
            
        }
        catch _ {
            
        }
    }
   
    // go to Landing controller
    @objc func gotoMenu(userid:String,deviceid:String)
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let rootView = storyBoard.instantiateViewController(withIdentifier: "Main") as! MainViewController
        rootView.deviceId = deviceid
        rootView.userId = userid
        rootView.uuid = self.uuid
        let navigationController = UINavigationController(rootViewController: rootView)
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

}
